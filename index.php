<?php
$json = file_get_contents('php://input');
if($json != ''){
  $data = json_decode($json);
  $output = [];
  $output['result'] = 'ok';
  switch($data->action){
    case 'save':
      file_put_contents('projects/'.$data->filename, json_encode($data->page));
      break;
    case 'listing':
      $files = array_diff(scandir('projects'), array('..', '.'));
      $output['files'] = $files;
      break;
    case 'delete':
      if(!unlink('projects/'.$data->filename)) $output['result'] = 'ko';

  }

  echo json_encode($output);
  exit();
}
?>
<!doctype html>
<html>
  <head>
    <title>Layout 01</title>
    <meta charset="utf-8">
    <link href="styles.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/vanilla-smoothie@2.2.2/dist/vanilla-smoothie.min.js"></script>
    <script src="classes/ui.class.js"></script>
    <script src="classes/cursor.class.js"></script>
    <script src="classes/page.class.js"></script>
    <script src="classes/projection.class.js"></script>
    <script src="classes/segment.class.js"></script>
    <script src="classes/surface.class.js"></script>

  </head>

  <body>
    <section class="draggable" id="ui">
      <header class="handle">
      </header>
      <div class="content">
        <div id="ui-page">
          <header>Page</header>
          <aside class="pannel" id="page-loading-pannel">
            <ul>
            </ul>
          </aside>
          <div>
            <input type="button" data-context="page" data-action="new" value="new">
            <input type="button" data-context="page" data-action="save" value="save">
            <input type="button" data-context="page" data-action="saveas" value="save as">
            <input type="button" data-context="page" data-action="load" value="load">
          </div>
          <div>
            <label for="page-width">width</label>
            <input type="number" id="page-width" data-context="page" data-action="change-width">
            <label for="page-height">height</label>
            <input type="number" id="page-height">
            <input type="button" data-context="page" data-action="apply" value="apply">
          </div>
          <div>
              <label for="page-border-width">border width (mm)</labeL>
              <input type="number" id="page-border-width" min="0" value="2" step="0.01" data-context="page" data-action="change-border" >
              <label for="page-border-overflow">border overflow</label>
              <input type="checkbox" id="page-border-overflow">

          </div>
        </div>
        <div id="ui-view">
          <header>View</header>
          <label for="view-scale">scale</label>
          <input type="range" id="view-scale" min="1" max="200" value="100" data-context="view" data-action="scale">
          <output for="view-scale" id="view-scale-output">100</output>
          <label for="view-animate-direction">animate direction</label>
          <select id="view-animate-direction">
            <option value="forward">forward</option>
            <option value="backward">backward</option>
          </select>
          <label for="view-animate-framerate">animate framerate</label>
          <input type="number" id="view-animate-framerate" min="0.01" step="0.01" value="1">
          <input type="button" data-context="view" data-action="animate" value="animate">
          <label for="view-scroll-target">scroll target</label>
          <input type="text" id="view-scroll-target">
          <label for="view-scroll-time">scroll time</label>
          <input type="number" id="view-scroll-time" min="1">
          <input type="button" data-context="view" data-action="scroll" value="scroll!">


        </div>
        <div id="ui-mode">
          <header>Mode</header>
          <input type="button" class="active" value="+" data-context="mode" data-action="new">
          <input type="button" value="-" data-context="mode" data-action="delete">
          <input type="button" value="⇆" data-context="mode" data-action="move">
        </div>
        <div id="ui-surface" class="disabled">
          <div>
            <header>Surface</header>
            <select id="surface-fill" data-context="surface" data-action="fill">
              <option value="none"></option>
              <?php

              $files = array_diff(scandir('images'), array('..', '.'));
              foreach($files as $file):
              ?>
              <option value="images/<?php echo $file; ?>"><?php echo $file; ?></option>

              <?php
              endforeach;
              ?>
            </select>
            <label for="surface-repeat">repeat</label>
            <input type="checkbox" id="surface-repeat" data-context="surface" data-action="repeat" checked>

            <label for="surface-scale">scale (%)</label>
            <input type="number" id="surface-scale" value="100" data-context="surface" data-action="scale">


            <input type="button" id="surface-autoscale" data-context="surface" data-action="autoscale" value="autoscale (based on width)">

            <label for="surface-xtranslate">x translation (mm)</label>
            <input type="number" id="surface-xtranslate" value="0" data-context="surface" data-action="xtranslate">

            <label for="surface-ytranslate">y translation (mm)</label>
            <input type="number" id="surface-ytranslate" value="0" data-context="surface" data-action="ytranslate">
            <label for="surface-refpoint">reference point<label>
            <select id="surface-refpoint" data-context="surface" data-action="refpoint">
              <option value="0">top left</option>
              <option value="1">top right</option>
              <option value="2">bottom right</option>
              <option value="3">bottom left</option>
            </select>
            <label for="surface-bgoffset-distance">Background offset effect: distance</label>
            <input type="number" id="surface-bgoffset-distance" value="0" step="0.1">
            <label for="surface-bgoffset-limit">Background offset effect: limit</label>
            <input type="number" id="surface-bgoffset-limit" value="2" min="2" step="1">
            <label for="surface-bgoffset-direction">Background offset effect: direction</label>
            <select id="surface-bgoffset-direction">
              <option value="-1">up</option>
              <option value="1">down</option>

            </select>
            <input type="button" data-context="surface" data-action="bgoffset" value="apply">
          </div>
          <div>
          <label for="surface-split-number">Split surface : number of divisions</label>
          <input type="number" id="surface-split-number" value="2" min="2">
          <label for="surface-split-axis">Split surface: axis</label>
          <select id="surface-split-axis">
            <option value="x">x</option>
            <option value="y">y</option>
          </select>
          <label for="surface-split-acceleration">Split surface: acceleration</label>
          <input type="number" id="surface-split-acceleration" value="0" step="0.1">

          <input type="button" data-context="surface" data-action="split" value="apply">
          </div>

        </div>
      </div>
    </section>




    <section id="workbench">

    </section>

    <section id="prototypes">
    </section>
    <script src="tools.js"></script>
    <script src="script.js"></script>

  </body>
</html>
