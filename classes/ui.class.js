function UI(elmt){
  this.elmt = elmt;
  this.mode = 'new';
  this.currentFile = null;

  this.initEvents = function(){
    var inputs = this.elmt.querySelectorAll('input');

    for(var i = 0; i < inputs.length; i++){
      if(inputs[i].hasAttribute('data-action'))
        inputs[i].onchange = this.triggerAction.bind(this);
    }
    var buttons = this.elmt.querySelectorAll('input[type="button"]');
    for(var i = 0; i < buttons.length; i++)
      buttons[i].onclick = this.triggerAction.bind(this);

    var sliders = this.elmt.querySelectorAll('input[type="range"]');
    for(var i = 0; i < sliders.length; i++){
      sliders[i].oninput = this.sliderInput.bind(this);
    }
    var selects = this.elmt.querySelectorAll('select');
    for(var i = 0; i < selects.length; i++){
      selects[i].onchange = this.triggerAction.bind(this);
    }
  }

  this.sliderInput = function(e){
    //console.log(e.target.id);
    var output = this.elmt.querySelector('output[for="'+e.target.id+'"]');
    //console.log(output);
    output.innerHTML = e.target.value;
  }
  this.triggerAction = function(e){
    var action = e.target.getAttribute('data-action');
    var context = e.target.getAttribute('data-context');
    var form = this.elmt.querySelector('#ui-'+context);
    console.log(action+' '+context);
    switch(context){
      case 'page':
        switch(action){
          case 'new':
            if(this.newPage()){
              this.page.buildTemplate();
            }
            return;
          case 'save':
            this.savePage();
            return;
          case 'saveas':
            this.savePage(true);
            return;
          case 'load':
            this.togglePannel('page-loading-pannel');
            return;
          case 'load-file':
            this.loadFile(e.target.getAttribute('data-file'));
            return;
          case 'delete':
            this.deletePage(e.target.getAttribute('data-file'));
            return;
          case 'change-border':
            this.changeBorder();
            return;
          case 'change-width':
            this.changeWidth();

        }
        return;
      case 'view':
        switch(action){
          case 'scale':
            this.page.setScale(e.target.value);
            return;
          case 'animate':
            var options = {'direction':form.querySelector('#view-animate-direction').value,
                         'framerate':form.querySelector('#view-animate-framerate').value
                        }
            this.page.animate(options);
            return;
          case 'scroll':
            var options = {'target':form.querySelector('#view-scroll-target').value,
                           'time':form.querySelector('#view-scroll-time').value
                          }
            this.page.scroll(options);
        }
        return;
      case 'mode':
        this.mode = action;
        return;
      case 'surface':
        switch(action){
          case 'repeat':
            this.page.surfaces.selected[0].setBackgroundProperty(action, e.target.checked);
            return;
          case 'scale':
          case 'xtranslate':
          case 'ytranslate':
          case 'refpoint':
            this.page.surfaces.selected[0].setBackgroundProperty(action, e.target.value);

            return;
          case 'autoscale':
            var scaleF = this.page.surfaces.selected[0].autoScaleBackground();
            this.updateMenu('surface', this.page.surfaces.selected[0]);
            return;
          case 'fill':
            if(e.target.value == 'none'){
              this.page.surfaces.selected[0].removeBackground();
            }
            else{
              var options = {'repeat':form.querySelector('#surface-repeat').checked,
                             'scale':form.querySelector('#surface-scale').value,
                             'xtranslate':form.querySelector('#surface-xtranslate').value,
                             'ytranslate':form.querySelector('#surface-ytranslate').value,
                             'refpoint':form.querySelector('#surface-refpoint').value
                            }
              this.page.surfaces.selected[0].setBackground(e.target.value, options);
            }
            return;

          case 'split':
            var options = {'divisions':form.querySelector('#surface-split-number').value,
                           'axis':form.querySelector('#surface-split-axis').value,
                           'acceleration':form.querySelector('#surface-split-acceleration').value

                          }
            this.page.splitSurface(this.page.surfaces.selected[0], options);
            return;
          case 'bgoffset':
            var options = {
              'distance':form.querySelector('#surface-bgoffset-distance').value,
              'limit':form.querySelector('#surface-bgoffset-limit').value,
              'direction':form.querySelector('#surface-bgoffset-direction').value
            }
            this.page.bgOffset(this.page.surfaces.selected[0], options);
        }
    }
  }
  this.showMenu = function(menu){
    this.elmt.querySelector('#ui-'+menu).classList.remove('disabled');
  }
  this.hideMenu = function(menu){
    this.elmt.querySelector('#ui-'+menu).classList.add('disabled');
  }
  this.updateMenu = function(menu, object){

    if(menu == 'surface'){
      var menuElmt = this.elmt.querySelector('#ui-'+menu);

      if(object.hasOwnProperty('background') && object.background.hasOwnProperty('url')){

        menuElmt.querySelector('select#surface-fill').value = object.background.url;
        menuElmt.querySelector('input#surface-repeat').checked = object.background.repeat;
        menuElmt.querySelector('input#surface-scale').value = object.background.scale;
        menuElmt.querySelector('input#surface-xtranslate').value = object.background.position.x;
        menuElmt.querySelector('input#surface-ytranslate').value = object.background.position.y;
      }
    }
  }
  this.changeBorder = function(){
    var borderW = this.elmt.querySelector('#page-border-width').value;
    var borderO = this.elmt.querySelector('#page-border-overflow').checked;
    console.log('TODO: SET BORDER');
    //this.page.setBorder(borderW, borderO);
  }
  this.changeWidth = function(){
    var pw = this.elmt.querySelector('#page-width').value;
    this.elmt.querySelector('#page-border-width').value = +(pw * 0.006).toFixed(1);

  }
  this.loadPage = function(data){
    console.log(data);
    this.elmt.querySelector('#page-width').value = data.width;
    this.elmt.querySelector('#page-height').value = data.height;
    this.elmt.querySelector('#page-border-width').value = data.segmentWidth;
    this.elmt.querySelector('#page-border-width').checked = data.segmentOverflow;
    this.elmt.querySelector('#view-scale').value = data.scale;
    this.elmt.querySelector('#view-scale-output').innerHTML = data.scale;
    this.newPage();
    this.page.setScale(data.scale);
    this.page.loadSegments(data.segments);
    this.page.loadSurfaces(data.surfaces);
    //this.page.loadGraph(data.segments, data.surfaces);

  }

  this.newPage = function(){
    var pw = this.elmt.querySelector('#page-width').value;
    var ph = this.elmt.querySelector('#page-height').value;

    if(pw == '' || ph == ''){
      alert('You must specify page\'s dimensions');
      return false;
    }
    var borderW = this.elmt.querySelector('#page-border-width').value;
    var borderO = this.elmt.querySelector('#page-border-overflow').checked;

    if(this.page != null && !this.page.saved){
      if(confirm('The current page is not saved. Continue anyway?')){
        this.page = new Page({'width':pw, 'height':ph, 'segmentWidth':Number(borderW), 'segmentOverflow':borderO});
        this.currentFile = 'null';
        return true;
        //parent.page = this.page;
      }
      return false;
    }
    this.currentFile = 'null';
    this.page = new Page({'width':pw, 'height':ph, 'segmentWidth':Number(borderW), 'segmentOverflow':borderO});
    return true;
    //parent.page = this.page;
  }
  this.savePage = function(newFile = false){
    if(this.fileName == null || newFile == true){
      this.fileName = prompt('Enter filename:')+'.json';

    }
    var pageData = this.page.getData();

    fetch('index.php', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({'action':'save', 'filename':this.fileName, 'page':pageData})
      }).then(res => res.json())
      .then(res => {
        if(res.result == 'ok'){
          this.page.saved = true;
          this.getAndUpdateFilesList();
        }
      });

  }
  this.deletePage = function(file){
    fetch('index.php', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({'action':'delete', 'filename':file})
      }).then(res => res.json())
      .then(res => this.getAndUpdateFilesList());


  }
  this.togglePannel = function(id){
    var pannel = this.elmt.querySelector('#'+id);
    if(pannel.style.display == 'block')
      pannel.style.display = 'none';
    else {
      pannel.style.display = 'block';
    }
  }
  this.loadFile = function(file){
    fetch('projects/'+file, {
      method: 'GET',
      headers: {
        'pragma':'no-cache',
        'cache-control':'no-cache'
      }
    }).then(res => res.json())
    .then(res => {this.loadPage(res); this.fileName = file;});
  }
  this.getAndUpdateFilesList = function(){
    fetch('index.php', {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({'action':'listing'})
      }).then(res => res.json())
      .then(res => this.updateFilesList(res.files));

  }
  this.updateFilesList = function(list){
    var ul = this.elmt.querySelector('#page-loading-pannel ul');
    ul.innerHTML = '';

    for(var i in list){
      var li = document.createElement('li');

      var delElmt = document.createElement('input');
      delElmt.type="button";
      delElmt.setAttribute('data-action', 'delete');
      delElmt.setAttribute('data-context', 'page');
      delElmt.setAttribute('data-file', list[i]);

      delElmt.value="delete";
      delElmt.onclick = this.triggerAction.bind(this);
      var loadElmt = delElmt.cloneNode();
      loadElmt.setAttribute('data-action', 'load-file');
      loadElmt.onclick = this.triggerAction.bind(this);
      loadElmt.value= list[i];

      li.appendChild(loadElmt);
      li.appendChild(delElmt);
      ul.appendChild(li);

    }
  }
  this.getAndUpdateFilesList();
  this.initEvents();

}
