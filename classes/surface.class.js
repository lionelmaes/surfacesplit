function Surface(data, page){
  this.segments = {};
  this.page = page;
  this.background = {};
  this.refpoints = [{'x':'left', 'y':'top'}, {'x':'right', 'y':'top'},
                    {'x':'right', 'y':'bottom'}, {'x':'left', 'y':'bottom'}];
  this.init = function(data){
    this.id = data.id;
    if(!this.page.segmentOverflow){
      this.left = data.top[0].x1 + this.page.segmentWidth / 2;
      this.top = data.top[0].y1 + this.page.segmentWidth / 2;
    }else{
      this.left = data.top[0].x1;
      this.top = data.top[0].y1
    }
    this.width = this.computeWidth(data.top);

    this.height = this.computeHeight(data.left);

    this.selected = false;

    this.segments.top = data.top;
    this.segments.bottom = data.bottom;
    this.segments.right = data.right;
    this.segments.left = data.left;

    this.buildHTML();
    this.initEvents();
  }
  this.pushSegment = function(border, segment){
    this.segments[border].push(segment);
  }
  this.replaceSegment = function(list, from, to){
    for(var i in this.segments[list]){

      if(this.segments[list][i] == from){
        //console.log('lets replace segment');
        //console.log(from);
        if(Array.isArray(to)){
          //console.log(to);
          this.segments[list].splice(i, 1);
          for(var j = to.length-1; j >= 0; j--){
            this.segments[list].splice(i, 0, to[j]);
          }

        }
        else {
          this.segments[list][i] = to;
        }
        return;
      }
    }
  }

  this.getSegmentFromPos = function(border, pos){

    if(border == 'top' || border == 'bottom')
      axis = 'x';
    else
      axis = 'y';

    for(var i in this.segments[border]){
      if(pos >= this.segments[border][i][axis+'1'] && pos <= this.segments[border][i][axis+'2']){
        return this.segments[border][i];
      }
    }
    return false;
  }

  this.getNextSegmentsOnBorder = function(border, segment, remove){

    for(var i in this.segments[border]){
      if(this.segments[border][i] == segment){


        if(i < this.segments[border].length - 1){

          if(remove)
            return this.segments[border].splice(i);
          else
            return this.segments[border].slice(i);
        }
        else{
          //il n'y a pas de segment après celui qu'on cherche, on renvoie un tableau vide (après l'avoir supprimé si besoin)
           if(remove){
             this.segments[border].splice(i);

           }
           return [];

        }
      }
    }
    return [];
  }

  this.setSegment = function(list, segment){
    this.segments[list] = segment;
  }

  this.computeWidth = function(topSegments){

    var x1 = topSegments[0].x1;
    var x2 = topSegments[topSegments.length - 1].x2;
    if(!this.page.segmentOverflow)
      return (x2-x1 - this.page.segmentWidth < 0)?0:x2 - x1 - this.page.segmentWidth;
    return (x2-x1 <= 0)?1:x2-x1;

  }
  this.computeHeight = function(leftSegments){
    var y1 = leftSegments[0].y1;
    var y2 = leftSegments[leftSegments.length - 1].y2;
    if(!this.page.segmentOverflow)
      return (y2 - y1 - this.page.segmentWidth < 0)?0:y2 - y1 - this.page.segmentWidth;
    return (y2 - y1 <=0)?1:y2 - y1;

  }
  this.setWidth = function(width){

    if(!this.page.segmentOverflow){
      this.width = (width - this.page.segmentWidth / 2 < 0)?0:width - this.page.segmentWidth / 2;

    }else
      this.width = width;
    this.elmt.style.width = this.width+'mm';
  }
  this.setHeight = function(height){

    if(!this.page.segmentOverflow)
      this.height = (height - this.page.segmentWidth / 2 < 0)?0:height - this.page.segmentWidth / 2;
    else
      this.height = height;

    this.elmt.style.height = this.height+'mm';

  }
  this.buildHTML = function(){
    this.elmt = document.createElement('div');
    this.elmt.id = 'surface-'+this.id;
    this.elmt.classList.add('surface');
    this.elmt.style.left = this.left+'mm';
    this.elmt.style.top = this.top+'mm';
    this.elmt.style.height = this.height+'mm';
    this.elmt.style.width = this.width+'mm';
  }
  this.relate = function(){
    this.elmt.classList.add('related');
  }
  this.unrelate = function(){
    this.elmt.classList.remove('related');
  }
  this.select = function(){
    this.selected = true;
    this.elmt.classList.add('selected');
  }
  this.unselect = function(){
    this.selected = false;
    this.elmt.classList.remove('selected');
  }
  this.initEvents = function(){
    this.elmt.onmouseenter = function(e){
        if(this.page.projection.active)return;
        parent.cursor.show();
        parent.cursor.setMode('surface');
        this.elmt.classList.add('highlight');
        for(var i in this.segments){
          for(var j in this.segments[i]){
            this.segments[i][j].relate();
          }
        }
    }.bind(this);
    this.elmt.onmouseleave = function(){
      this.elmt.classList.remove('highlight');
      for(var i in this.segments){
        for(var j in this.segments[i]){
          this.segments[i][j].unrelate();
        }
      }
    }.bind(this);
    this.elmt.onclick = function(e){
      this.page.selectSurface(this, {x:e.offsetX, y:e.offsetY});
    }.bind(this);

  }
  this.hide = function(hideFrame){
    this.elmt.style.visibility = 'hidden';
    if(hideFrame){
      for(var i in this.segments){
        for(var j in this.segments[i]){
          this.segments[i][j].hide();
        }
      }
    }
  }
  this.show = function(showFrame){
    this.elmt.style.visibility = 'visible';
    if(showFrame){
      for(var i in this.segments){
        for(var j in this.segments[i]){
          this.segments[i][j].show();
        }
      }
    }
  }
  this.getBackgroundSize = function(url, fallback){
    var image = new Image();
    image.src = url;
    image.onload = function() {
      this.background.size = {
          width: image.naturalWidth,
          height: image.naturalHeight
      };
      fallback();
    }.bind(this);
  }

  this.setBackground = function(url, options){
    this.background.url = url;
    this.background.position = {'x':0, 'y': 0};
    this.background.refpoint = 0;

    this.background.scale = 100;
    this.elmt.style.backgroundImage = 'url('+url+')';
    console.log('OPTIONS');
    console.log(options);
    this.getBackgroundSize(url, function(){this.setBackgroundProperties(options);}.bind(this));
  }


  this.removeBackground = function(){
    this.background = {};
    this.elmt.style.backgroundImage = 'none';
  }
  this.setBackgroundProperties = function(options){
    for(var key in options){
      this.setBackgroundProperty(key, options[key]);
    }
  }
  this.setBackgroundProperty = function(prop, value){
    switch(prop){
      case 'repeat':
        this.background.repeat = value;
        if(value){
          this.elmt.style.backgroundRepeat = 'repeat';
        }else{
          this.elmt.style.backgroundRepeat = 'no-repeat';
        }
        return;
      case 'position':
          this.background.position = value;
          this.elmt.style.backgroundPosition = this.background.position.x+'mm '+this.background.position.y+'mm';
          return;
      case 'xtranslate':

        this.background.position.x = value;

        this.elmt.style.backgroundPosition = this.refpoints[this.background.refpoint].x+' '+this.background.position.x+'mm '+
                                             this.refpoints[this.background.refpoint].y+' '+this.background.position.y+'mm';
        return;
      case 'ytranslate':

          this.background.position.y = value;
          this.elmt.style.backgroundPosition = this.refpoints[this.background.refpoint].x+' '+this.background.position.x+'mm '+
                                               this.refpoints[this.background.refpoint].y+' '+this.background.position.y+'mm';

        return;
      case 'refpoint':
        this.background.refpoint = value;
        this.elmt.style.backgroundPosition = this.refpoints[value].x+' '+this.background.position.x+'mm '+
                                             this.refpoints[value].y+' '+this.background.position.y+'mm';
        return;
      case 'scale':
        this.background.scale = value;
        if(this.background.hasOwnProperty('size')){
          var w = this.background.size.width / 100 * value;
          var h = this.background.size.height / 100 * value;
          console.log(w+'px '+h+'px');
          this.elmt.style.backgroundSize = w+'px '+h+'px';
        }
    }
  }
  this.bgOffset = function(distance){
    var currentOffset = this.background.position.x;
    currentOffset += distance;
    this.setBackgroundProperty('xtranslate', currentOffset);
  }
  this.autoScaleBackground = function(){
    var targetW = xToPx((this.width - this.background.position.x * 2)+'mm');
    console.log('targetW'+targetW);
    console.log('backgroundwidth'+this.background.size.width);
    var scaleF = (targetW / this.background.size.width)*100;
    this.setBackgroundProperty('scale', scaleF);
    return scaleF;
  }
  this.getData = function(){
    var borders = ['top', 'bottom', 'right', 'left'];
    var segmentsOutput = {};
    for(var i in borders){
      segmentsOutput[borders[i]] = [];
      for(var j in this.segments[borders[i]]){
        segmentsOutput[borders[i]].push(this.segments[borders[i]][j].id);
      }
    }

    return {
      'id':this.id,
      'left': this.left,
      'top': this.top,
      'width':this.width,
      'height':this.height,
      'segments':segmentsOutput,
      'background':this.background
    }
  }

  this.init(data);

}
