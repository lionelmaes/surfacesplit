function Page(data){
  this.surfaces = {all:[], selected:[]};
  this.segments = {x:[], y:[], selected:[]};

  this.segmentsC = 0;
  this.surfacesC = 0;
  this.saved = false;
  this.init = function(data){
    this.workBench = document.querySelector('#workbench');
    while(this.workBench.firstChild) this.workBench.removeChild(this.workBench.firstChild);
    this.width = data.width;
    this.height = data.height;

    this.segmentWidth = data.segmentWidth;
    this.segmentOverflow = data.segmentOverflow;
    this.buildHTML();
    this.initEvents();
    this.projection = new Projection(this);
    this.elmt.appendChild(this.projection.elmt);
    this.scale = 100;
  }


  this.initEvents = function(){
    this.workBench.onmouseover = function(){

      parent.cursor.setMode('workbench');
      parent.cursor.show();
    }.bind(this);
    this.workBench.onmouseleave = function(){
      parent.cursor.hide();
    }.bind(this);

    this.elmt.onmousemove = function(e){
      if(this.projection.active){
        var mousePos = relativeCoords(e, this.elmt);
        this.projection.projectToSegment(mousePos);
      }
    }.bind(this);

  }
  this.buildTemplate = function(){
    //make the frame
    var frame = {'top':[this.createSegment({'x1':0, 'y1':0, 'x2':this.width, 'y2': 0, 'static' : true})],
                 'right':[this.createSegment({'x1':this.width, 'y1':0,
                                             'x2':this.width, 'y2': this.height, 'static' : true})],
                 'bottom':[this.createSegment({'x1':0, 'y1':this.height,
                                              'x2':this.width, 'y2': this.height, 'static' : true})],
                 'left':[this.createSegment({'x1':0, 'y1':0, 'x2':0, 'y2': this.height, 'static' : true})]};

    var surface = this.createSurface(frame);


    //console.log(this.segments);

  }

  this.buildHTML = function(){
    this.workBench.innerHTML = '<div class="page" style="'+
                               'width:'+this.width+'mm;'+
                               'height:'+this.height+'mm"></div>';

    this.elmt = this.workBench.querySelector('.page');

  }
  this.setScale = function(scale){
    this.elmt.style.transform = 'scale('+scale/100+')';
    this.scale = scale;
  }
  this.animate = function(options){
    for(var i in this.surfaces.all){
      this.surfaces.all[i].hide(true);
    }
    if(options.direction == 'backward'){
      var i = this.surfaces.all.length - 1;
      var direction = -1;
    }else{
      var i = 0;
      var direction = 1;
    }
    window.setTimeout(function(){this.showSurface(i, options.framerate, direction);}.bind(this), 1000);

  }
  this.scroll = function(options){
    //const vanillaSmoothie = new VanillaSmoothie();
    vanillaSmoothie.scrollTo('#'+options.target, {
      duration: Number(options.time)
    });

  }
  this.showSurface = function(i, framerate, direction){

    if(i >= this.surfaces.all.length || i < 0)
      return;
    this.surfaces.all[i].show(true);
    var parent = this;
    i+=direction;
    window.setTimeout(function(){this.showSurface(i, framerate, direction);}.bind(parent), framerate * 1000);
  }
  this.getData = function(){
    var output = {
                    'width':this.width,
                    'height':this.height,
                    'segmentWidth':this.segmentWidth,
                    'segmentOverflow':this.segmentOverflow,
                    'scale':this.scale,
                    'surfaces':[],
                    'segments':{'x':[], 'y':[]}
                 };
    for(var i in this.surfaces.all){
      output.surfaces.push(this.surfaces.all[i].getData());
    }
    for(var i in this.segments.x){
      output.segments.x.push(this.segments.x[i].getData());
    }
    for(var i in this.segments.y){
      output.segments.y.push(this.segments.y[i].getData());
    }
    return output;

  }

  //#region surfaces
  this.createSurface = function(data){
    this.surfacesC++;
    if(!data.hasOwnProperty('id'))
      data.id = this.surfacesC;
    else if(data.id > this.surfacesC)
      this.surfacesC = data.id;

    var surface = new Surface(data, this);
    this.elmt.appendChild(surface.elmt);
    var bordersRel = {'top':'forward', 'left':'forward', 'right':'back','bottom':'back'};
    for(var i in bordersRel){
      for(var j in data[i]){
        data[i][j].surfaces[bordersRel[i]] = surface;
      }
    }


    for(var i in this.surfaces.all){
      if(this.surfaces['all'][i].top >= surface.top
      && this.surfaces['all'][i].left >= surface.left){
        this.surfaces['all'].splice(i, 0, surface);
        return surface;
      }
    }
    this.surfaces['all'].push(surface);
    return surface;
  }

  this.loadSurfaces = function(surfaces){

    for(var i in surfaces){
      var data = {'id':surfaces[i].id};
      for(var border in surfaces[i].segments){
        data[border] = [];
        for(var j in surfaces[i].segments[border]){
          data[border].push(this.getSegmentFromId(surfaces[i].segments[border][j]));

        }
      }
      var surface = this.createSurface(data);
      if(surfaces[i].background.hasOwnProperty('url')){
        surface.setBackground(surfaces[i].background.url, surfaces[i].background);
      }
    }

  }
  this.bgOffset = function(surface, options){
    var direction = Number(options.direction);
    var distance = - Number(options.distance);
    var limit = Number(options.limit);
    var currentOffset = Number(surface.background.position.x);


    var counter = 0;
    for(var i = this.getSurfaceInd(surface.id) + direction; i >= 0 && i < this.surfaces.all.length; i = i + direction){
      //console.log(i);
      if(counter % limit == 0){
        distance = -distance;
      }
      currentOffset += distance;
      //console.log(currentOffset);

      this.surfaces.all[i].setBackgroundProperty('xtranslate', currentOffset);
      counter++;
    }
  }
  this.splitSurface = function(surface, options){

    if(options.axis == 'x'){//vertical
      var aBorder = 'top';
      var bBorder = 'bottom';
      var dimension = 'width';
      var borderPos = 'left';
      var axis = 'x';
      var oAxis = 'y';
    }
    else{
      var aBorder = 'left';
      var bBorder = 'right';
      var dimension = 'height';
      var borderPos = 'top';
      var axis = 'y';
      var oAxis = 'x';
    }


    var currentPos = 0;
    var steps = [];

    if(options.acceleration >= 0)
      acceleration = 1.0 + Number(options.acceleration);
    else
      acceleration = 1.0 - Number(options.acceleration);

    //console.log('acceleration');
    //console.log(acceleration);
    for(var i = 1; i < options.divisions; i++){
      steps.push(i / options.divisions);
    }
    if(options.acceleration < 0){
      steps.reverse();
    }
    //console.log(steps);

    var dimension = surface[dimension];
    var position = surface[borderPos];

    for(var i = 0; i < steps.length; i++){

      if(options.acceleration >= 0){
        var pos = Math.pow(steps[i], acceleration) * dimension + position;
        //console.log(Math.pow(steps[i], acceleration));
      }else{
        //console.log(1-steps[i]);
        var pos = (1-Math.pow(steps[i], acceleration)) * dimension + position;

        //console.log(1-Math.pow(steps[i], acceleration));
      }
      console.log('computed pos'+ pos);


      //then we need to find the segment containing this position from the frame of the surface
      var aSegment = surface.getSegmentFromPos(aBorder, pos);

      var bSegment = surface.getSegmentFromPos(bBorder, pos);
      var data = {
        'static':false,
      };
      data[axis+'1'] = data[axis+'2'] = pos;
      data[oAxis+'1'] = aSegment[oAxis+'1'];
      data[oAxis+'2'] = bSegment[oAxis+'1'];

      var cSegment = this.createSegment(data);
      var splitSegments = this.splitSegments(aSegment, bSegment, cSegment);
      //console.log(splitSegments);
      surface = this.splitSurfaceFromSegments(aSegment, bSegment, cSegment, splitSegments);
      this.removeSegment(aSegment);
      this.removeSegment(bSegment);

    }

    this.unselectSurfaces();


  }

  this.splitSurfaceFromSegments = function(aSegment, bSegment, cSegment, splitSegments){
    //we need to split the old surface (forward surface from asegment)
    var aSurface = aSegment.surfaces.forward;

    //reduce the height or the width of the old surface


    if(aSegment.axis == 'x'){

      aSurface.setWidth(cSegment.x1 - aSurface.left);

      var topNextSegments = aSurface.getNextSegmentsOnBorder('top', aSegment, true);
      var bottomNextSegments = aSurface.getNextSegmentsOnBorder('bottom', bSegment, true);
      var bSurface = this.createSurface({'left':[cSegment], 'top':[splitSegments.a2].concat(topNextSegments), 'right':aSurface.segments.right, 'bottom':[splitSegments.b2].concat(bottomNextSegments)});


      aSurface.pushSegment('top', splitSegments.a1);
      aSurface.pushSegment('bottom', splitSegments.b1);

      aSurface.setSegment('right', [cSegment]);

      if(aSegment.surfaces.back != null){

        aSegment.surfaces.back.replaceSegment('bottom', aSegment, [splitSegments.a1, splitSegments.a2]);

      }
      if(bSegment.surfaces.forward != null){

        bSegment.surfaces.forward.replaceSegment('top', bSegment, [splitSegments.b1, splitSegments.b2]);

      }
    }else{
      aSurface.setHeight(cSegment.y1 - aSurface.top);

      var rightNextSegments = aSurface.getNextSegmentsOnBorder('right', bSegment, true);
      var leftNextSegments = aSurface.getNextSegmentsOnBorder('left', aSegment, true);

      var bSurface = this.createSurface({'top':[cSegment], 'left':[splitSegments.a2].concat(leftNextSegments), 'bottom':aSurface.segments.bottom, 'right':[splitSegments.b2].concat(rightNextSegments)});


      aSurface.pushSegment('left', splitSegments.a1);
      aSurface.pushSegment('right', splitSegments.b1);

      aSurface.setSegment('bottom', [cSegment]);

      if(aSegment.surfaces.back != null){

        aSegment.surfaces.back.replaceSegment('right', aSegment, [splitSegments.a1, splitSegments.a2]);


      }
      if(bSegment.surfaces.forward != null){

        bSegment.surfaces.forward.replaceSegment('left', bSegment, [splitSegments.b1, splitSegments.b2]);

      }



    }
    //et là il faut encore attacher les surfaces aux segments
    for(var i in bSurface.segments.top){
      bSurface.segments.top[i].surfaces.forward = bSurface;
    }

    for(var i in bSurface.segments.bottom){
      bSurface.segments.bottom[i].surfaces.back = bSurface;
    }

    for(var i in bSurface.segments.right){
      bSurface.segments.right[i].surfaces.back = bSurface;
    }
    for(var i in bSurface.segments.left){
      bSurface.segments.left[i].surfaces.forward = bSurface;
    }

    splitSegments.a1.surfaces.forward = aSurface;
    splitSegments.a2.surfaces.forward = bSurface;

    splitSegments.b1.surfaces.back = aSurface;
    splitSegments.b2.surfaces.back = bSurface;


    cSegment.surfaces.back = aSurface;
    cSegment.surfaces.forward = bSurface;

    console.log('*******************');
    console.log('aSurface:');
    console.log(aSurface);
    console.log('bSurface');
    console.log(bSurface);


    if(aSurface.background.hasOwnProperty('url')){
      bSurface.setBackground(aSurface.background.url, aSurface.background);
    }

    return bSurface;
  }
  this.selectSurface = function(surface, offset){
    if(parent.ui.mode == 'new'){

      if(this.surfaces.selected.length == 0){
        //launch the loading content tool
        this.surfaces.selected.push(surface);
        surface.select();
        parent.ui.showMenu('surface');

        parent.ui.updateMenu('surface', surface);

      }else{
        parent.ui.hideMenu('surface');

        this.unselectSurfaces();
      }
    }
  }
  this.unselectSurfaces = function(){
    for(var i in this.surfaces.selected){
      this.surfaces.selected[i].unselect();
    }

    this.surfaces.selected = [];
  }
  this.getSurfaceInd = function(id){
    for(var i in this.surfaces.all){
      if(this.surfaces.all[i].id == id){
        return Number(i);
      }
    }
  }
  //#endregion
  //#region segments
  this.createSegment = function(data){
    this.unselectSegments();
    this.segmentsC++;
    //console.log('segment id='+this.segmentsC);
    if(!data.hasOwnProperty('id'))
      data.id = this.segmentsC;
    else if(data.id > this.segmentsC)
      this.segmentsC = data.id;

    data.segmentWidth = this.segmentWidth;
    var segment = new Segment(data, this);
    var oAxis = (segment.axis == 'y')?'x':'y';
    var posA = segment[oAxis+1];
    this.elmt.appendChild(segment.elmt);

    for(var i in this.segments[segment.axis]){
      var posB = this.segments[segment.axis][i][oAxis+1];
      if(posB > posA){
        this.segments[segment.axis].splice(i, 0, segment);
        return segment;
      }

    }
    this.segments[segment.axis].push(segment);
    return segment;

  }
  this.loadSegments = function(segmentsData){
    for(var axis in segmentsData){
      for(var i in segmentsData[axis]){
        this.createSegment(segmentsData[axis][i]);
      }

    }
  }
  this.removeSegment = function(segment){
    segment.remove();

    for(var i in this.segments[segment.axis]){
      if(this.segments[segment.axis][i] == segment){
        this.segments[segment.axis].splice(i, 1);
      }
    }
  }
  this.splitSegments = function(aSegment, bSegment, cSegment){
    //divide the a and b segments with c segment

    //console.log(aSegment);
    //console.log(bSegment);

    if(cSegment.axis == 'x'){
      //TODO: shorten this code!
      var dY1 = aSegment.y1;
      var dY2 = cSegment.y1;
      var eY1 = cSegment.y1;
      var eY2 = aSegment.y2;

      var fY1 = bSegment.y1;
      var fY2 = cSegment.y1;
      var gY1 = cSegment.y1;
      var gY2 = bSegment.y2;

      var a1 = this.createSegment({x1:aSegment.x1, x2:aSegment.x2, y1:dY1, y2:dY2, static:false});
      var a2 = this.createSegment({x1:aSegment.x1, x2:aSegment.x2, y1:eY1, y2:eY2, static:false});

      var b1 = this.createSegment({x1:bSegment.x1, x2:bSegment.x2, y1:fY1, y2:fY2, static:false});
      var b2 = this.createSegment({x1:bSegment.x1, x2:bSegment.x2, y1:gY1, y2:gY2, static:false});
    }
    else{
      var dX1 = aSegment.x1;
      var dX2 = cSegment.x1;
      var eX1 = cSegment.x1;
      var eX2 = aSegment.x2;

      var fX1 = bSegment.x1;
      var fX2 = cSegment.x1;
      var gX1 = cSegment.x1;
      var gX2 = bSegment.x2;

      var a1 =  this.createSegment({x1:dX1, x2:dX2, y1:aSegment.y1, y2:aSegment.y2, static:false});
      var a2 = this.createSegment({x1:eX1, x2:eX2, y1:aSegment.y1, y2:aSegment.y2, static:false});


      var b1 = this.createSegment({x1:fX1, x2:fX2, y1:bSegment.y1, y2:bSegment.y2, static:false});
      var b2 = this.createSegment({x1:gX1, x2:gX2, y1:bSegment.y1, y2:bSegment.y2, static:false});

    }
    a1.surfaces.back = aSegment.surfaces.back;
    a2.surfaces.back = aSegment.surfaces.back;

    b1.surfaces.forward = bSegment.surfaces.forward;
    b2.surfaces.forward = bSegment.surfaces.forward;
    splits = {'a1':a1, 'a2':a2, 'b1':b1, 'b2':b2};

    return splits;

  }
  this.unselectSegments = function(){
    for(var i in this.segments.x){
      this.segments.x[i].unselect();
    }
    for(var i in this.segments.y){
      this.segments.y[i].unselect();
    }
    this.segments.selected = [];
  }

  this.selectSegment = function(segment, offset){
    if(parent.ui.mode == 'new'){

      if(this.segments.selected.length == 0){
        //launch the projection tool
        this.segments.selected.push(segment);
        segment.select();
        this.projection.active = true;

      }else{

        this.projection.active = false;
        this.unselectSegments();
      }
    }
  }
  this.getSegmentFromId = function(id){

    for(var axis in this.segments){
      for(var i in this.segments[axis]){

        if(this.segments[axis][i].id == id)
          return this.segments[axis][i];
      }
    }
    return false;
  }
  this.getSegmentInd = function(id, axis){
    for(var i in this.segments[axis]){
      if(this.segments[axis][i].id == id){
        return Number(i);
      }
    }
  }
  //#endregion


  this.init(data);
}
