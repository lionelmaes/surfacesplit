
function Cursor(){
  this.init = function(){
    this.buildHTML();
    this.initEvents();
  }
  this.initEvents = function(){
    document.body.onmousemove = function(e){
      this.elmt.style.left = e.pageX - this.elmt.offsetWidth/2+'px';
      this.elmt.style.top = e.pageY - this.elmt.offsetHeight/2+'px';
    }.bind(this);
  }
  this.buildHTML = function(){
    this.elmt = document.createElement('div');
    this.elmt.id="cursor";
    document.body.appendChild(this.elmt);
  }
  this.setPosition = function(x, y){
    this.elmt.style.left = x - this.elmt.offsetWidth/2+'px';
    this.elmt.style.top = y - this.elmt.offsetHeight/2+'px';
  }
  this.setDimensions = function(w, h = w){
    this.elmt.style.width = w + 'px';
    this.elmt.style.height = h + 'px';

  }
  this.setMode = function(object){
    this.elmt.setAttribute('data-mode', parent.ui.mode+'-'+object);
  }
  this.show = function(){
    this.elmt.style.display = "block";
  }
  this.hide = function(){
    this.elmt.style.display = "none";
  }
  this.init();
}
