function Segment(data, page){
  this.surfaces = {'back':null, 'forward':null};
  this.page = page;
  this.selected = false;
  this.init = function(){
    console.log(data);
    this.x1 = data.x1;

    this.x2 = Number(data.x2);
    this.y1 = Number(data.y1);
    this.y2 = Number(data.y2);

    this.static = data.static;

    this.id = data.id;
    this.segmentWidth = Number(data.segmentWidth);

    this.buildHTML();
    this.initEvents();
  }
  this.buildHTML = function(){
    this.elmt = document.createElement('div');
    this.elmt.classList.add('segment');
    this.elmt.id = 'segment-'+this.id;
    if(this.x1 == this.x2){
      this.elmt.classList.add('y');
      this.axis = 'y';
      this.elmt.style.width =  this.segmentWidth+'mm';
      this.elmt.style.height = this.y2 - this.y1 + this.segmentWidth+'mm';
    }
    else{
      this.elmt.classList.add('x');
      this.axis = 'x';
      this.elmt.style.height =  this.segmentWidth+'mm';
      this.elmt.style.width = this.x2 - this.x1 + this.segmentWidth +'mm';
      console.log(this.x2);
      console.log(this.x1);
      console.log(this.segmentWidth);
    }
    this.elmt.style.left = this.x1 - this.segmentWidth/2 +'mm';
    this.elmt.style.top = this.y1 - this.segmentWidth/2+'mm';
  }
  this.select = function(){
    this.selected = true;
    this.elmt.classList.add('selected');
  }
  this.unselect = function(){
    this.selected = false;
    this.elmt.classList.remove('selected');
  }
  this.relate = function(){
    this.elmt.classList.add('related');
  }
  this.unrelate = function(){
    this.elmt.classList.remove('related');
  }
  this.initEvents = function(){
    this.elmt.onmouseenter = function(e){
        if(this.page.projection.active)return;
        parent.cursor.show();
        
        parent.cursor.setMode('segment');
        this.elmt.classList.add('highlight');
        if(this.surfaces.back != null)
          this.surfaces.back.relate();
        if(this.surfaces.forward != null)
          this.surfaces.forward.relate();
    }.bind(this);
    this.elmt.onmouseleave = function(){
      this.elmt.classList.remove('highlight');
      if(this.surfaces.back != null)
        this.surfaces.back.unrelate();
      if(this.surfaces.forward != null)
        this.surfaces.forward.unrelate();
    }.bind(this);
    this.elmt.onclick = function(e){
      this.page.selectSegment(this, {x:e.offsetX, y:e.offsetY});
    }.bind(this);
  }
  this.hide = function(hideSurfaces = false){
    this.elmt.style.visibility = 'hidden';
  }
  this.show = function(showSurfaces = false){
    this.elmt.style.visibility = 'visible';
  }
  this.intersects = function(mousePos){
    //check if the perpendicular segment given by a mouse position intersects with segment
    if(this.axis == 'x'){
      return mousePos.x > this.elmt.offsetLeft && mousePos.x < this.elmt.offsetLeft + this.elmt.offsetWidth;
    }else{
      return mousePos.y > this.elmt.offsetTop && mousePos.y < this.elmt.offsetTop + this.elmt.offsetHeight;
    }
  }
  this.remove = function(){
    this.elmt.remove();
  }
  this.getData = function(){
    return {
      'x1':this.x1,
      'x2':this.x2,
      'y1':this.y1,
      'y2':this.y2,
      'static':this.static,
      'id':this.id,
      'segmentWidth':this.segmentWidth,
      'surfaces':{
        'back':(this.surfaces.back!=null)?this.surfaces.back.id:null,
        'forward':(this.surfaces.forward!=null)?this.surfaces.forward.id:null
      }
    }
  }
  this.init(data);
}
