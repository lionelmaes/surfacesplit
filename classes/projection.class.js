function Projection(page){
  this.active = false;
  this.page = page;
  this.init = function(){
    this.buildHTML();
    this.initEvents();
  }
  this.initEvents = function(){
    this.elmt.onclick = function(e){

      var data = {'static':false};
      if(this.axis == 'x'){
        //use the aSegment and bSegment coordinates to make the new one
        data.x1 = this.aSegment.x1;
        data.x2 = this.bSegment.x1;

        //convert projection position in px to mm to make segment
        data.y1 = data.y2 = this.elmt.offsetTop / this.page.elmt.offsetHeight * this.page.height;

      }else{

        data.y1 = this.aSegment.y1;
        data.y2 = this.bSegment.y1;

        data.x1 = data.x2 = this.elmt.offsetLeft / this.page.elmt.offsetWidth * this.page.width;
      }
      var cSegment = this.page.createSegment(data);
      var splitSegments = this.page.splitSegments(this.aSegment, this.bSegment, cSegment);
      //console.log(splitSegments);
      this.page.splitSurfaceFromSegments(this.aSegment, this.bSegment, cSegment, splitSegments);
      this.page.removeSegment(this.aSegment);
      this.page.removeSegment(this.bSegment);

      this.hide();
      this.active = false;
    }.bind(this);
  }

  this.buildHTML = function(){
    this.elmt = document.createElement('div');
    this.elmt.id = 'projection';

  }
  this.hide = function(){
    this.elmt.style.display = 'none';
  }
  this.projectToSegment = function(mousePos){
    mousePos.x = mousePos.x / (this.page.scale/100);
    mousePos.y = mousePos.y / (this.page.scale/100);
    var aSegment = this.page.segments.selected[0];




    var segments = this.page.segments[aSegment.axis];
    //unselect all but the current one
    for(var i in segments){
      if(segments[i] == aSegment) continue;
      segments[i].unselect();
    }

    var aSegmentInd = this.page.getSegmentInd(aSegment.id, aSegment.axis);

    if(aSegment.axis == 'y'){

      //limit mousePos to aSegment
      if(mousePos.y > aSegment.elmt.offsetTop + aSegment.elmt.offsetHeight)
        mousePos.y = aSegment.elmt.offsetTop + aSegment.elmt.offsetHeight;
      else if(mousePos.y < aSegment.elmt.offsetTop)
        mousePos.y = aSegment.elmt.offsetTop;


      var direction = (mousePos.x <= aSegment.elmt.offsetLeft)?-1:1;
      this.axis = 'x';
    }else{
      //limit mousePos to aSegment
      if(mousePos.x > aSegment.elmt.offsetLeft + aSegment.elmt.offsetWidth)
        mousePos.x = aSegment.elmt.offsetLeft + aSegment.elmt.offsetWidth;
      else if(mousePos.x < aSegment.elmt.offsetLeft)
        mousePos.x = aSegment.elmt.offsetLeft;

      var direction = (mousePos.y <= aSegment.elmt.offsetTop)?-1:1;
      this.axis = 'y';
    }
    if(direction == -1 && aSegmentInd == 0)
      return;
    if(direction == 1 && aSegmentInd >= segments.length)
      return;

    //look up for bSegment -> must be in the segments array after or before the aSegment depending of the direction
    for(var i = aSegmentInd + direction; i >= 0 && i < segments.length; i += direction){
      //console.log(i);
      if(segments[i].intersects(mousePos)){
        var bSegment = segments[i];
        break;
      }
    }
    if(bSegment == undefined){
      console.log('unable to find bSegment');
      return;
    }
    //var bSegment = (direction == -1)?segments[Number(aSegmentInd)-1]:segments[Number(aSegmentInd)+1];
    bSegment.select();

    //sort the aSegment and bSegment (so that we always project to positive)
    if(direction == 1){
      this.aSegment = aSegment;
      this.bSegment = bSegment;
    }
    else{
      this.bSegment = aSegment;
      this.aSegment = bSegment;
    }

    this.drawProjection(mousePos);


  }
  this.drawProjection = function(mousePos){

    this.elmt.style.display = 'block';
    if(this.axis == 'x'){

      this.elmt.style.left = this.aSegment.elmt.offsetLeft + 'px';
      this.elmt.style.width = this.bSegment.elmt.offsetLeft - this.aSegment.elmt.offsetLeft + this.aSegment.elmt.offsetWidth + 'px';
      this.elmt.style.height = '1px';
      this.elmt.style.top = mousePos.y + 'px';
    }else{

      this.elmt.style.top = this.aSegment.elmt.offsetTop + 'px';
      this.elmt.style.height = this.bSegment.elmt.offsetTop - this.aSegment.elmt.offsetTop + this.aSegment.elmt.offsetHeight + 'px';
      this.elmt.style.width = '1px';
      this.elmt.style.left = mousePos.x + 'px';

    }
  }
  this.init();


}
