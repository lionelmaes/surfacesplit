/*
#Split screen layout tool#
#A. Surface and segments
1. we take the screen or the page as a surface
2. we can draw a segment between two segments of this surface (the perimeter segments)
   (horizontal one between two vertical ones and vice versa)
3. when a segment is drawn, the surface is divided in two
   (thus creating a new surface)
4. if the segment intersects with another drawn segment, it divides this segment in two
   thus a surface border can contain multiple segments
5. if a segment is moved, the two adjacents surfaces are adapted and if the segments intersects with
   other segments, they are adapted too (or created)

#B. Content and surface
1. one can insert content in a surface
2. the content is moveable, resizable, rotatable
3. when a segment is deleted, one of the related surface is deleted.
   the other surface is resized to include the deleted one.

#C. UI
1. create a new page
2. save it
3. delete it
4. zoom in, zoom out
5. cursor mode: create segments, move segments, delete segments

#C. Classes
1. the Segment class
2. the Surface class
3. the Media class (can be color, svg file, bitmap file)
4. the UI class

#C. Storage
A layout can be stored in json format containing the page object, a list of segment
objects and a list of surface objects.

TODO: check the order of the segments array

*/

var ui = new UI(document.getElementById('ui'));
var cursor = new Cursor();
