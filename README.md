# SurfaceSplit

A layout prototyping tool in html/js. Divide a surface with segments, generating other surfaces that can be filled with image/text/svg.