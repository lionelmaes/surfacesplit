
var dragItems = document.querySelectorAll('.draggable');
for (var i = 0; i < dragItems.length; i++) {
  draggable(dragItems[i]);
}

function draggable(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (elmnt.querySelector('header.handle')) {
    // if present, the header is where you move the DIV from:

    elmnt.querySelector('header.handle').onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

function relativeCoords ( event, target) {
  var bounds = target.getBoundingClientRect();
  var x = event.clientX - bounds.left;
  var y = event.clientY - bounds.top;
  x = (x > bounds.right- bounds.left)?bounds.right- bounds.left:x;
  y = (y > bounds.bottom - bounds.top)?bounds.bottom - bounds.top:y;
  return {x: x, y: y};
}

function xToPx(x) {
    var div = document.createElement('div');
    div.style.display = 'block';
    div.style.height = x;
    document.body.appendChild(div);
    var px = parseFloat(window.getComputedStyle(div, null).height);
    div.parentNode.removeChild(div);
    return px;
}
